package BattleShipUI;

import BattleShipUI.game.GameCreator;
import BattleShipUI.util.Const;
import BattleShipUI.util.contract.ILayout;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Main extends Application implements ILayout {
    private Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource(Const.SRC_GAME_MAIN_LAYOUT));

        initialization(root);

        primaryStage.setTitle(BattleShipUI.util.Const.APPLICATION_NAME);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @Override
    public void initialization(Parent root) {
        Button player = (Button) root.lookup(Const.ID_PLAYER);
        Button computer = (Button) root.lookup(Const.ID_COMPUTER);

        GameCreator gameCreator = new GameCreator();

        player.setOnMouseClicked(event -> openWindow(gameCreator, Const.PLAYER));

        computer.setOnMouseClicked(event -> openWindow(gameCreator, Const.COMPUTER));
    }

    public void openWindow(GameCreator gameCreator, int flag) {
        try {
            gameCreator.start(stage);
            gameCreator.passingData(flag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

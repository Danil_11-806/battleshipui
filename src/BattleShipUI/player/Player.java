package BattleShipUI.player;

import BattleShipUI.model.Cell;
import BattleShipUI.model.Ship;
import BattleShipUI.util.Const;

import java.util.ArrayList;
import java.util.List;

public class Player {
    protected String name;
    protected List<Ship> shipList;
    protected List<Cell> cellList;
    protected int health;

    public Player(String name, List<Ship> shipList) {
        this.name = name;
        this.shipList = shipList;
        this.health = Const.HEALTH;
        this.cellList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Ship> getShipList() {
        return shipList;
    }

    public boolean isLose() {
        return health == 0;
    }

    public void damage() {
        health--;
    }

    public void addCell(Cell cell) {
        cellList.add(cell);
    }

    public List<Cell> getCellList() {
        return cellList;
    }

    public boolean isCellExist(int cellX, int cellY) {
        for (Cell cell : cellList) {
            if (cell.getCellX() == cellX && cell.getCellY() == cellY) {
                return true;
            }
        }
        return false;
    }
}
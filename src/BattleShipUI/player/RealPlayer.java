package BattleShipUI.player;

import BattleShipUI.model.Ship;

import java.util.List;

public class RealPlayer extends Player {
    public RealPlayer(String name, List<Ship> shipList) {
        super(name, shipList);
    }
}

//package BattleShipUI.player;
//
//import BattleShipUI.model.Cell;
//import BattleShipUI.model.Ship;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ComputerPlayer extends Player {
//    int count = -1;
//    private List<Boolean> hitList = new ArrayList<>();
//
//    public ComputerPlayer(String name, List<Ship> shipList) {
//        super(name, shipList);
//        //Сгенирировать набор кораблей
//        shipList = new ArrayList<>();
//        shipList.add(new Ship(1,1,3, false));
//        shipList.add(new Ship(1,2,4, false));
//        setShipList(shipList);
//    }
//
//    //Должен вернуть кординаты для удара по карте врага
//    public Cell getCell() {
//        int x = (int) (Math.random() * 10 + 1);
//        int y = (int) (Math.random() * 10 + 1);
//        while (super.isCellExist(x, y)) {
//            x = (int) (Math.random() * 10 + 1);
//            y = (int) (Math.random() * 10 + 1);
//        }
//        return new Cell(x, y);
//    }
//
//    public void setShipList(List<Ship> shipList) {
//        super.shipList = shipList;
//    }
//
//    //Добавляет в список hitList правильнось кординат, false(Промах), true(Попадание)
//    public void addHitList(boolean isHit) {
//        hitList.add(isHit);
//    }
//}

package BattleShipUI.player;

import BattleShipUI.model.Cell;
import BattleShipUI.model.Ship;
import BattleShipUI.util.Const;
import BattleShipUI.util.GameMap;

import java.util.ArrayList;
import java.util.List;

public class ComputerPlayer extends Player {
    public static void main(String[] args) {
        new ComputerPlayer("test", null);
    }
    private int frandom = 0;
    private int srandom = 0;

    private int startX = 0;
    private int startY = 0;

    private int strikes = 0;

    private GameMap gameMap;
    private List<Cell> cellList;

    public ComputerPlayer(String name, List<Ship> shipList) {
        super(name, shipList);
        super.shipList = new ArrayList<>();
        cellList = new ArrayList<>();
        gameMap = new GameMap();
        generateShip();
    }

    private void generateShip() {
        int[] shipLength = Const.SHIP;
        int count = 0;
        while (count < shipLength.length) {
            int x = 1 + (int) (Math.random() * 10);
            int y = 1 + (int) (Math.random() * 10);
            boolean rotate = Math.random() > 0.5;
            Ship ship = new Ship(x, y, shipLength[count], rotate);
            if (isEmpty(cellList, ship) &&
                    ((!rotate && x + shipLength[count] - 1 <= Const.LAYER_SIZE) ||
                            (rotate && y + shipLength[count] - 1 <= Const.LAYER_SIZE))) {
                shipList.add(ship);
                addBorder(ship.isRotate(), ship.getCellX(), ship.getCellY(), ship.getLength());
                count++;
            }
        }
    }

    private void addBorder(boolean isRotate, int x, int y, int len) {
        if (!isRotate) {
            for (int i = -1; i < len + 2; i++) {
                cellList.add(new Cell(x+i,y));
            }
            for (int i = -1; i < len + 2; i++) {
                cellList.add(new Cell(x+i,y-1));
            }
            for (int i = -1; i < len + 2; i++) {
                cellList.add(new Cell(x+i,y+1));
            }
        } else {
            for (int i = -1; i < len + 2; i++) {
                cellList.add(new Cell(x,i + y));
            }
            for (int i = -1; i < len + 2; i++) {
                cellList.add(new Cell(x-1,i + y));
            }
            for (int i = -1; i < len + 2; i++) {
                cellList.add(new Cell(x+1,i + y));
            }
        }
    }

    public boolean isEmpty(List<Cell> cellList, Ship ship) {
        for (Cell cell: cellList) {
            if (cell.getCellX() == ship.getCellX() && ship.getCellY() <= cell.getCellY() && ship.getCellY() + ship.getLength() > cell.getCellY()) {
                return false;
            } else if (cell.getCellY() == ship.getCellY() && ship.getCellX() <= cell.getCellX() && ship.getCellX() + ship.getLength() > cell.getCellX()) {
                return false;
            }
        }
        return true;
    }

    public Cell getCell(List<Ship> firstPlayer, boolean lastHit) {
        if (lastHit || strikes != 0) {
            if (lastHit && strikes == 0) {
                startX = frandom;
                startY = srandom;
                strikes = 4;
            }
            if (strikes > 0) {
                if (strikes == 4) { //up
                    if (srandom == 1 || isCellExist(frandom, srandom - 1)) {
                        strikes--;
                        frandom = startX;
                        srandom = startY;
                    } else {
                        Cell newCell = up();
                        if (currentHit(firstPlayer, frandom, srandom - 1)) {
                            srandom -= 1;
                        }

                        return newCell;
                    }
                }
                if (strikes == 3) { //down
                    if (srandom == 10 || isCellExist(frandom, srandom + 1)) {
                        strikes--;
                    } else {
                        Cell newCell = down();

                        if (currentHit(firstPlayer, frandom, srandom + 1)) {
                            srandom += 1;
                        }

                        return newCell;
                    }
                }
                if (strikes == 2) { //right
                    if (frandom == 10 || isCellExist(frandom + 1, srandom)) {
                        frandom = startX;
                        srandom = startY;
                        strikes--;
                    } else {
                        Cell newCell = right();

                        if (currentHit(firstPlayer, frandom + 1, srandom)) {
                            frandom += 1;
                        }

                        return newCell;
                    }
                }

                if (strikes == 1) { //left
                    if (frandom == 1 || isCellExist(frandom - 1, srandom)) {
                        strikes--;
                    } else {
                        Cell newCell = left();

                        if (currentHit(firstPlayer, frandom - 1, srandom)) {
                            frandom -= 1;
                        }

                        return newCell;
                    }
                }
            }
        }
        strikes = 0;
        startX = 0;
        startY = 0;
        cellList.add(getRandomCell());
        return new Cell(frandom, srandom);
    }

    public Cell up() {
        int x = frandom;
        int y = srandom - 1;
        cellList.add(new Cell(x, y));
        return new Cell(x, y);
    }


    public Cell right() {
        int x = frandom + 1;
        int y = srandom;
        cellList.add(new Cell(x, y));
        return new Cell(x, y);
    }


    public Cell down() {
        int x = frandom;
        int y = srandom + 1;
        cellList.add(new Cell(x, y));
        return new Cell(x, y);
    }

    public Cell left() {
        int x = frandom - 1;
        int y = srandom;
        cellList.add(new Cell(x, y));
        return new Cell(x, y);
    }


    public Cell getRandomCell() {
        frandom = 1 + (int) (Math.random() * 10);
        srandom = 1 + (int) (Math.random() * 10);
        Cell newCell = new Cell(frandom, srandom);
        while (isCellExist(frandom, srandom)) {
            frandom = 1 + (int) (Math.random() * 10);
            srandom = 1 + (int) (Math.random() * 10);
            newCell = new Cell(frandom, srandom);
        }
        return newCell;
    }

    public boolean currentHit(List<Ship> list, int x, int y) {
        return !gameMap.isFieldNotEmpty(x, y, 1, list);
    }
}
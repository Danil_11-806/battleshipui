package BattleShipUI.util;

public class Const {
    public static final int PLAYER = 1;
    public static final int HEALTH = 20;
    public static final int COMPUTER = 0;
    public static final int CELL_SIZE = 25;
    public static final int[] SHIP = {4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
    public static final int LAYER_SIZE = 10;

    public static final String STEP = "player move: ";
    public static final String WINNER = " is winner";
    public static final String APPLICATION_NAME = "Battle Ship";

    public static final String SRC_GAME_LAYOUT = "../res/layout/game.fxml";
    public static final String SRC_GAME_MAIN_LAYOUT = "res/layout/game_main.fxml";
    public static final String SRC_GAME_OVER_LAYOUT = "../res/layout/game_over.fxml";
    public static final String SRC_GAME_CREATOR_LAYOUT = "../res/layout/game_creator.fxml";

    public static final String ID_GO = "#go";
    public static final String ID_GAME = "#name";
    public static final String ID_ERROR = "#error";
    public static final String ID_WINNER = "#winner";
    public static final String ID_PLAYER = "#player";
    public static final String ID_CANVAS = "#canvas";
    public static final String ID_REMOVE = "#remove";
    public static final String ID_COMPUTER = "#computer";
    public static final String ID_TURN_OVER = "#turnOver";
    public static final String COMPUTER_NAME = "Computer";
    public static final String ID_TEXT_FIRST = "#firstText";
    public static final String ID_TEXT_SECOND = "#secondText";
    public static final String ID_CANVAS_FIRST = "#canvasFirst";
    public static final String ID_CANVAS_SECOND = "#canvasSecond";
}

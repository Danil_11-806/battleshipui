package BattleShipUI.util;

import BattleShipUI.model.Ship;
import BattleShipUI.util.contract.IGameMap;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.List;

public class GameMap implements IGameMap {
    private GraphicsContext context;

    @Override
    public void setContext(GraphicsContext context) {
        this.context = context;
    }

    @Override
    public void drawMap() {
        context.setFill(Color.WHITE);
        context.setStroke(Color.GRAY);
        for (int i = 0; i < Const.LAYER_SIZE; i++) {
            for (int j = 0; j < Const.LAYER_SIZE; j++) {
                context.fillRect(Const.CELL_SIZE * i, Const.CELL_SIZE * j, Const.CELL_SIZE, Const.CELL_SIZE);
                context.strokeRect(Const.CELL_SIZE * i, Const.CELL_SIZE * j, Const.CELL_SIZE, Const.CELL_SIZE);
            }
        }
    }

    @Override
    public void clearPartMap(int cellX, int cellY, int length, boolean isRotate) {
        for (int i = 0; i < length; i++) {
            context.setFill(Color.WHITE);
            context.setStroke(Color.GRAY);
            int[] xy = defineXY(cellX, cellY, isRotate, i);
            context.fillRect(xy[0], xy[1], Const.CELL_SIZE, Const.CELL_SIZE);
            context.strokeRect(xy[0], xy[1], Const.CELL_SIZE, Const.CELL_SIZE);
        }
    }

    @Override
    public void drawShip(int cellX, int cellY, int length, boolean isRotate) {
        for (int i = 0; i < length; i++) {
            context.setFill(Color.GRAY);
            int[] xy = defineXY(cellX, cellY, isRotate, i);
            context.fillRect(xy[0], xy[1], Const.CELL_SIZE, Const.CELL_SIZE);
        }
    }

    @Override
    public int[] defineXY(int cellX, int cellY, boolean isRotate, int i) {
        int[] xy = new int[2];
        if (!isRotate) {
            xy[0] = Const.CELL_SIZE * (cellX + i - 1);
            xy[1] = Const.CELL_SIZE * (cellY - 1);
        } else {
            xy[0] = Const.CELL_SIZE * (cellX - 1);
            xy[1] = Const.CELL_SIZE * (cellY + i - 1);
        }
        return xy;
    }

    @Override
    public boolean isFieldNotEmpty(int cellX, int cellY, int length, List<Ship> shipList) {
        boolean isFieldNotEmpty = true;
        for (Ship ship : shipList) {
            if (!ship.isRotate()) {
                if (cellY == ship.getCellY() && cellX >= ship.getCellX() && cellX <= ship.getCellX() + ship.getLength() - 1) {
                    isFieldNotEmpty = false;
                }
            } else {
                if (cellX == ship.getCellX() && cellY >= ship.getCellY() && cellY <= ship.getCellY() + ship.getLength() - 1) {
                    isFieldNotEmpty = false;
                }
            }
        }
        return isFieldNotEmpty;
    }

    @Override
    public void drawField(int cellX, int cellY, Paint color) {
        context.setFill(color);
        context.fillRect(cellX * 25 - 25, cellY * 25 - 25, 25, 25);
    }

    @Override
    public int defineCell(double cord) {
        int cell = 0;
        while (cord > 0) {
            cord = cord - Const.CELL_SIZE;
            cell++;
        }
        return cell;
    }
}

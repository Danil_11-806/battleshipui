package BattleShipUI.util.contract;

import BattleShipUI.model.Ship;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;

import java.util.List;

public interface IGameMap {
    void setContext(GraphicsContext context);

    void drawMap();

    void clearPartMap(int cellX, int cellY, int length, boolean isRotate);

    void drawShip(int cellX, int cellY, int length, boolean isRotate);

    int[] defineXY(int cellX, int cellY, boolean isRotate, int i);

    boolean isFieldNotEmpty(int cellX, int cellY, int length, List<Ship> shipList);

    void drawField(int cellX, int cellY, Paint color);

    int defineCell(double cord);
}

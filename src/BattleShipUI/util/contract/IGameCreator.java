package BattleShipUI.util.contract;

public interface IGameCreator {
    void rotateShip();

    void addShip(double x, double y);

    void fixShip();

    void removeShip();

    void createPlayer();
}

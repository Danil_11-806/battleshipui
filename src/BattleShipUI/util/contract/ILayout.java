package BattleShipUI.util.contract;

import javafx.scene.Parent;

public interface ILayout {
    void initialization(Parent root);
}

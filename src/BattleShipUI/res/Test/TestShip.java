package BattleShipUI.res.Test;

import BattleShipUI.model.Ship;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class TestShip {
    @Test
    public void testShipIsRotated() {
        Ship s = new Ship(0,0, 3, false);

        assertFalse(s.isRotate());
    }
}

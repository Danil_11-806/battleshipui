package BattleShipUI.res.Test;

import BattleShipUI.util.GameMap;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGame {
    @Test
    public void testDefineXY() {
        GameMap g = new GameMap();

        int expected = 3;
        int actual = g.defineCell(67.2);
        assertEquals(expected, actual);
    }
}

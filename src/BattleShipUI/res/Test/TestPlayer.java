package BattleShipUI.res.Test;

import BattleShipUI.model.Ship;
import BattleShipUI.player.Player;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class TestPlayer {
    @Test
    public void isPlayerLose() {
        List<Ship> shipList = new ArrayList<>();
        Player p = new Player("Test", shipList);
        for (int i = 0; i < 20; i++) {
            p.damage();
        }
        assertTrue(p.isLose());
    }
}

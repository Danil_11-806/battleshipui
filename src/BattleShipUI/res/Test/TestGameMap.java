package BattleShipUI.res.Test;


import BattleShipUI.model.Ship;
import BattleShipUI.util.GameMap;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestGameMap {
    GameMap m;
    List<Ship> shipList;

    @Before
    public void setUp() {
        m = new GameMap();
        shipList = new ArrayList<>();

        shipList.add(new Ship(5, 5, 1, false));
        shipList.add(new Ship(3, 7, 4, false));
    }

    @Test
    public void testIsFieldNotEmptyTrue() {
        assertTrue(m.isFieldNotEmpty(0, 0, 2, shipList));
    }

    @Test
    public void testIsFieldNotEmptyFalse() {
        assertFalse(m.isFieldNotEmpty(4, 7, 2, shipList));
    }
}

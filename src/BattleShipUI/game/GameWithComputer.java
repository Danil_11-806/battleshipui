package BattleShipUI.game;

import BattleShipUI.model.Cell;
import BattleShipUI.model.Ship;
import BattleShipUI.player.ComputerPlayer;
import BattleShipUI.player.Player;
import BattleShipUI.util.Const;
import BattleShipUI.util.GameMap;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class GameWithComputer extends AbstractGame {
    private Player firstPlayer;
    private ComputerPlayer secondPlayer;

    private Canvas firstMap;
    private Canvas secondMap;

    private GameMap gameMap;
    private boolean hit = false;

    @Override
    public void initialization(Parent root) {
        firstMap = (Canvas) root.lookup(Const.ID_CANVAS_FIRST);
        secondMap = (Canvas) root.lookup(Const.ID_CANVAS_SECOND);

        gameMap = new GameMap();

        firstPlayer = super.firstPlayer;
        secondPlayer = (ComputerPlayer) super.secondPlayer;

        gameMap.setContext(firstMap.getGraphicsContext2D());
        gameMap.drawMap();

        gameMap.setContext(secondMap.getGraphicsContext2D());
        gameMap.drawMap();

        secondMap.setOnMouseClicked(event -> strike(
                secondMap.getGraphicsContext2D(),
                gameMap.defineCell(event.getX()),
                gameMap.defineCell(event.getY()),
                secondPlayer));

        gameMap.setContext(firstMap.getGraphicsContext2D());
        for (Ship ship : secondPlayer.getShipList()) {
            gameMap.drawShip(ship.getCellX(), ship.getCellY(), ship.getLength(), ship.isRotate());
        }

        gameMap.setContext(firstMap.getGraphicsContext2D());
    }

    @Override
    protected void strike(GraphicsContext context, int cellX, int cellY, Player player) {
        gameMap.setContext(context);

        Paint paint = Color.GREY;
        boolean isHit = false;
        if (!gameMap.isFieldNotEmpty(cellX, cellY, 1, player.getShipList())) {
            paint = Color.RED;
            if (!firstPlayer.isCellExist(cellX, cellY)) {
                isHit = true;
                player.damage();
            }
        }

        gameMap.drawField(cellX, cellY, paint);

        if (!firstPlayer.isCellExist(cellX, cellY)) {
            firstPlayer.addCell(new Cell(cellX, cellY));
            if (!isHit) {
                nextStep(isHit);
            }
        }

        super.isGameOver();
    }

    private void nextStep(boolean isHit) {
        gameMap.setContext(firstMap.getGraphicsContext2D());
        isHit = true;

        while (isHit) {
            Paint paint = Color.GREY;
            Cell cell = secondPlayer.getCell(firstPlayer.getShipList(), hit);
            isHit = false;
            if (!gameMap.isFieldNotEmpty(cell.getCellX(), cell.getCellY(), 1, firstPlayer.getShipList())) {
                paint = Color.RED;
                isHit = true;
                firstPlayer.damage();
            }
            hit = isHit;
            secondPlayer.addCell(cell);

            gameMap.drawField(cell.getCellX(), cell.getCellY(), paint);
        }
    }
}
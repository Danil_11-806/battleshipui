package BattleShipUI.game;

import BattleShipUI.model.Cell;
import BattleShipUI.player.Player;
import BattleShipUI.util.Const;
import BattleShipUI.util.GameMap;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

public class GameWithPlayer extends AbstractGame {
    private Canvas firstMap;
    private Canvas secondMap;

    private Text firstText;
    private Text secondText;

    private GraphicsContext currentContext;
    private GameMap gameMap;
    private int step = 1;

    @Override
    public void initialization(Parent root) {
        firstMap = (Canvas) root.lookup(Const.ID_CANVAS_FIRST);
        secondMap = (Canvas) root.lookup(Const.ID_CANVAS_SECOND);

        firstText = (Text) root.lookup(Const.ID_TEXT_FIRST);
        secondText = (Text) root.lookup(Const.ID_TEXT_SECOND);

        gameMap = new GameMap();

        firstPlayer = super.firstPlayer;
        secondPlayer = super.secondPlayer;

        gameMap.setContext(firstMap.getGraphicsContext2D());
        gameMap.drawMap();

        gameMap.setContext(secondMap.getGraphicsContext2D());
        gameMap.drawMap();

        firstMap.setOnMouseClicked(event -> strike(
                firstMap.getGraphicsContext2D(),
                gameMap.defineCell(event.getX()),
                gameMap.defineCell(event.getY()),
                firstPlayer));
        secondMap.setOnMouseClicked(event -> strike(
                secondMap.getGraphicsContext2D(),
                gameMap.defineCell(event.getX()),
                gameMap.defineCell(event.getY()),
                secondPlayer));

        currentContext = secondMap.getGraphicsContext2D();
        secondText.setText(Const.STEP + firstPlayer.getName());
    }

    @Override
    protected void strike(GraphicsContext context, int cellX, int cellY, Player player) {
        if (currentContext != context) {
            return;
        }

        gameMap.setContext(context);

        Paint paint = Color.GRAY;
        boolean isHit = false;
        if (!gameMap.isFieldNotEmpty(cellX, cellY, 1, player.getShipList())) {
            paint = Color.RED;
            if (!player.isCellExist(cellX, cellY)) {
                player.damage();
            }
            isHit = true;
        }

        gameMap.drawField(cellX, cellY, paint);

        if (!player.isCellExist(cellX, cellY)) {
            player.addCell(new Cell(cellX, cellY));
            nextStep(isHit);
        }

        super.isGameOver();
    }

    private void nextStep(boolean isHit) {
        if (isHit) {
            return;
        }

        if (step % 2 != 0) {
            currentContext = firstMap.getGraphicsContext2D();
            firstText.setText(Const.STEP + secondPlayer.getName());
            secondText.setText(null);
        } else {
            currentContext = secondMap.getGraphicsContext2D();
            firstText.setText(null);
            secondText.setText(Const.STEP + firstPlayer.getName());
        }

        step += 1;
    }
}

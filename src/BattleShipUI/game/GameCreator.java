package BattleShipUI.game;

import BattleShipUI.model.Ship;
import BattleShipUI.player.ComputerPlayer;
import BattleShipUI.player.RealPlayer;
import BattleShipUI.util.Const;
import BattleShipUI.util.GameMap;
import BattleShipUI.util.contract.IGameCreator;
import BattleShipUI.util.contract.IGameMap;
import BattleShipUI.util.contract.ILayout;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;


public class GameCreator extends Application implements ILayout, IGameCreator {
    private TextField name;
    private Text error;

    private GameMap gameMap;
    private RealPlayer firstPlayer;
    private Stage stage;

    private int flag;
    private int lastCellX = 0;
    private int lastCellY = 0;
    private int lastLength = 0;
    private int shipCounter = 0;
    private boolean isRotate = false;
    private List<Ship> shipList = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource(Const.SRC_GAME_CREATOR_LAYOUT));

        initialization(root);

        primaryStage.setTitle(BattleShipUI.util.Const.APPLICATION_NAME);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void passingData(int flag) {
        this.flag = flag;
    }

    @Override
    public void initialization(Parent root) {
        Canvas canvas = (Canvas) root.lookup(Const.ID_CANVAS);
        ImageView turnOver = (ImageView) root.lookup(Const.ID_TURN_OVER);
        ImageView remove = (ImageView) root.lookup(Const.ID_REMOVE);
        Button go = (Button) root.lookup(Const.ID_GO);

        name = (TextField) root.lookup(Const.ID_GAME);
        error = (Text) root.lookup(Const.ID_ERROR);

        GraphicsContext context = canvas.getGraphicsContext2D();
        gameMap = new GameMap();
        gameMap.setContext(context);
        gameMap.drawMap();

        canvas.setOnMouseMoved(event -> addShip(event.getX(), event.getY()));

        canvas.setOnMouseClicked(event -> fixShip());

        remove.setOnMouseClicked(event -> removeShip());

        go.setOnMouseClicked(event -> createPlayer());

        turnOver.setOnMouseClicked(event -> rotateShip());
    }

    @Override
    public void rotateShip() {
        if (shipCounter != Const.SHIP.length) {
            gameMap.clearPartMap(lastCellX, lastCellY, lastLength, isRotate);
            isRotate = !isRotate;
        }
    }

    @Override
    public void addShip(double x, double y) {
        if (shipCounter < Const.SHIP.length) {
            int cellX = gameMap.defineCell(x);
            int cellY = gameMap.defineCell(y);

            gameMap.clearPartMap(lastCellX, lastCellY, lastLength, isRotate);
            gameMap.drawShip(cellX, cellY, BattleShipUI.util.Const.SHIP[shipCounter], isRotate);

            for (BattleShipUI.model.Ship ship : shipList) {
                gameMap.drawShip(ship.getCellX(), ship.getCellY(), ship.getLength(), ship.isRotate());
            }

            lastCellX = cellX;
            lastCellY = cellY;
            lastLength = BattleShipUI.util.Const.SHIP[shipCounter];
        }
    }

    @Override
    public void fixShip() {
        if (gameMap.isFieldNotEmpty(lastCellX, lastCellY, lastLength, shipList) &&
                ((!isRotate && lastCellX + lastLength - 1 <= Const.LAYER_SIZE) ||
                        (isRotate && lastCellY + lastLength - 1 <= Const.LAYER_SIZE))) {
            shipList.add(new Ship(lastCellX, lastCellY, lastLength, isRotate));
            shipCounter++;
            error.setVisible(false);
        } else {
            error.setVisible(true);
        }
    }

    @Override
    public void removeShip() {
        Ship ship = shipList.get(shipList.size() - 1);
        gameMap.clearPartMap(ship.getCellX(), ship.getCellY(), ship.getLength(), ship.isRotate());
        shipList.remove(ship);
        if (shipCounter != 0) {
            shipCounter--;
        }
    }

    @Override
    public void createPlayer() {
        if (shipCounter == Const.SHIP.length && !name.getText().equals("")) {
            try {
                if (flag == Const.PLAYER) {
                    if (firstPlayer == null) {
                        firstPlayer = new RealPlayer(name.getText(), shipList);
                        shipList = new ArrayList<>();
                        shipCounter = 0;
                        start(stage);
                    } else {
                        RealPlayer secondPlayer = new RealPlayer(name.getText(), shipList);
                        GameWithPlayer gameWithPlayer = new GameWithPlayer();
                        gameWithPlayer.start(stage);
                        gameWithPlayer.passingData(firstPlayer, secondPlayer);
                    }
                } else {
                    RealPlayer firstPlayer = new RealPlayer(name.getText(), shipList);
                    ComputerPlayer secondPlayer = new ComputerPlayer(Const.COMPUTER_NAME, null);
                    GameWithComputer gameWithComputer = new GameWithComputer();
                    gameWithComputer.start(stage);
                    gameWithComputer.passingData(firstPlayer, secondPlayer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

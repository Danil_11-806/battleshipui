package BattleShipUI.game;

import BattleShipUI.util.Const;
import BattleShipUI.util.contract.ILayout;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GameOver extends Application implements ILayout {
    private Text text;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(Const.SRC_GAME_OVER_LAYOUT));

        initialization(root);

        primaryStage.setTitle(Const.APPLICATION_NAME);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void passingData(String name) {
        text.setText(name + Const.WINNER);
    }

    @Override
    public void initialization(Parent root) {
        text = (Text) root.lookup(Const.ID_WINNER);
    }
}

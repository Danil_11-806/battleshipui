package BattleShipUI.game;

import BattleShipUI.player.Player;
import BattleShipUI.util.Const;
import BattleShipUI.util.contract.ILayout;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public abstract class AbstractGame extends Application implements ILayout {
    protected Stage stage;
    protected Player firstPlayer;
    protected Player secondPlayer;
    private Parent root;

    public abstract void initialization(Parent root);

    protected abstract void strike(GraphicsContext context, int cellX, int cellY, Player player);

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        root = FXMLLoader.load(getClass().getResource(Const.SRC_GAME_LAYOUT));

        primaryStage.setTitle(Const.APPLICATION_NAME);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    protected void isGameOver() {
        if (firstPlayer.isLose() || secondPlayer.isLose()) {
            GameOver gameOver = new GameOver();
            try {
                gameOver.start(stage);
                gameOver.passingData((firstPlayer.isLose()) ? secondPlayer.getName() : firstPlayer.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void passingData(Player firstPlayer, Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;

        initialization(root);
    }
}

package BattleShipUI.model;

public class Cell {
    private int cellX;
    private int cellY;

    public Cell(int cellX, int cellY) {
        this.cellX = cellX;
        this.cellY = cellY;
    }

    public int getCellX() {
        return cellX;
    }

    public int getCellY() {
        return cellY;
    }
}

package BattleShipUI.model;

public class Ship {
    private int cellX;
    private int cellY;
    private int length;
    private boolean isRotate;

    public Ship(int cellX, int cellY, int length, boolean isRotate) {
        this.cellX = cellX;
        this.cellY = cellY;
        this.length = length;
        this.isRotate = isRotate;
    }

    public int getCellX() {
        return cellX;
    }

    public int getCellY() {
        return cellY;
    }

    public int getLength() {
        return length;
    }

    public boolean isRotate() {
        return isRotate;
    }
}
